'use strict';

angular.module('jarvisApp')
	.config(function ($stateProvider) 
	{
		$stateProvider
		.state('/', {
			url: '/',
			template:'<home></home>'
		},
		{
			url: 'panel-10',
			template:'<portfolio></portfolio>'
		})
		.state('logout', {
			url: '/logout',
			template:'<logout></logout>'
		})
		.state('portfolio', {
			url: '/portfolio',
			template:'<portfolio></portfolio>'
		})
		.state('create-portfolio', {
			url: '/create-portfolio',
			template:'<create-portfolio></create-portfolio>'
		})
		.state('profile', {
			url: '/profile',
			template:'<profile></profile>'
		})
		.state('dashboard', {
			url: '/dashboard',
			template:'<dashboard></dashboard>'
		})
		.state('terms-and-conditions', {
			url: '/terms-and-conditions',
			template:'<terms></terms>'
		})
		.state('privacy', {
			url: '/privacy',
			template:'<privacy></privacy>'
		})
		.state('disclaimer', {
			url: '/disclaimer',
			template:'<disclaimer></disclaimer>'
		})
		.state('faq', {
			url: '/faq',
			template:'<faq></faq>'
		})
		.state('blog', {
			url: '/blog',
			template:'<blog></blog>'
		})
		.state('price', {
			url: '/price',
			template:'<price></price>'
		})
		.state('blogview', {
			url: '/blogview',
			template:'<blogview></blogview>'
		})
	});