'use strict';
angular.module('jarvisApp', ['ui.router','ngCookies','jarvisApp.util','rzModule','ngSlimScroll'])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
});

angular.module("jarvisApp").directive('ngLoading',function(){
	return {
		template:'<div class="ngloading"><i class="fa fa-cog fa-spin"></i> Loading...</div>'
	};
});

angular.module("jarvisApp").factory('httpRequestInterceptor', function ($cookies,Util) {
  return {
    request: function (config) {
      config.headers=config.headers||{};
      if($cookies.get('token') && Util.isSameOrigin(config.url)) {
        config.headers['Authorization'] = 'Bearer '+$cookies.get('token');
      }
      config.headers['Accept'] = 'application/json; charset=UTF-8';

      return config;
    }
  };
});

angular.module("jarvisApp").config(function ($httpProvider) {
  $httpProvider.interceptors.push('httpRequestInterceptor');
});

angular.module("jarvisApp").run(function($rootScope,$interval,$cookies,$location){
	$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
		if (typeof $cookies.get('token') === 'undefined') {
			//$location.url('/');
		}
	});
	$interval(()=>{$rootScope.clock = Date.now();},1000);
});

angular.module("jarvisApp").directive('ngModel', function() {
    return {
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {
            elem.on('blur', function() {
                ngModel.$dirty = true;
                scope.$apply();
				(ngModel.$invalid==true && ngModel.$dirty==true)?elem.addClass('has-error'):elem.removeClass('has-error');
            });
            
            ngModel.$viewChangeListeners.push(function() {
                ngModel.$dirty = false;
            });
            
            scope.$on('$destroy', function() {
                elem.off('blur');
            });	
        }
    }
});

angular.module("jarvisApp").directive('loading',   ['$http' ,function ($http)  
{  
     return {  
         restrict: 'A',  
         template: '<div class="loading-spiner"></div><div class="loader">Loading..</div>',  
         link: function (scope, elm, attrs)  
         {  
             scope.isLoading = function () {  
                 return $http.pendingRequests.length > 0;  
             };  
  
             scope.$watch(scope.isLoading, function (v)  
             {  
                 if(v){  
                     elm.show();  
                 }else{  
                     elm.hide();  
                 }  
             });  
         }  
     };  
}]); 




