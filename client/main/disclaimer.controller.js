'use strict';

(function () {

  class DisclaimerController {
         
    $onInit() {

      $(".navbar-nav > li > a").click(function(){
      $(".navbar-nav > li").children().removeClass('active');
      $(this).addClass('active');
      $("#toggle-nav-body").addClass('hide-on-mob');
    });
    
    
    $("#toggle-nav").click(function(){
      $("#toggle-nav-body").toggleClass('hide-on-mob');
    });
    $(window).scroll(function() {
      if ($(this).scrollTop() > 150){
        $(".arrow_up").removeClass('hidden');
      }
      else
      {
        $(".arrow_up").addClass('hidden');
      }
    });
    $("#next").click(function(){
      $("#sign-up-panel-1").addClass('hidden');
      $("#sign-up-panel-2").removeClass('hidden');
    });
    $("#back").click(function(){
      $("#sign-up-panel-1").removeClass('hidden');
      $("#sign-up-panel-2").addClass('hidden');
    });
    this.$rootScope.userinfo=(window.sessionStorage["userInfo"])?JSON.parse(window.sessionStorage["userInfo"]):null;
    if(this.$rootScope.userinfo==null)
    {
      this.$cookies.remove('token');
      $(".showLogBtn").addClass('hidden');
      $(".showAuthBtn").removeClass('hidden');
    }
    else
    {
      $(".showLogBtn").removeClass('hidden');
      $(".showAuthBtn").addClass('hidden');
    }
    setTimeout(function(){$('.dropdown-trigger').dropdown(/* {hover:true} */);},0);
    $('.tooltipped').tooltip();
        
          document.addEventListener('DOMContentLoaded', function() {
              var elems = document.querySelectorAll('.tooltipped');
              var instances = M.Tooltip.init(elems, options);
      });

  }
    goTerms() {
      window.location.href = "terms-and-conditions";
    }
    down()
		{
			$('html,body').animate({
				scrollTop: 550},
				'slow');
		}
		totop()
		{
			$('html,body').animate({
				scrollTop: 0},
				'slow');
		}
  }
  angular.module('jarvisApp')
    .component('disclaimer', {
      templateUrl: './client/main/disclaimer.html',
      controller: DisclaimerController
    });
})();