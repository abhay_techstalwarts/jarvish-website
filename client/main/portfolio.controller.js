(function(){
class PortfolioController
{
	constructor($scope,$http,$state,$rootScope,$cookies,$timeout)
	{
		this.$http = $http;
		this.$state = $state;
		this.$rootScope=$rootScope;
		this.$cookies=$cookies;
		this.$timeout=$timeout;
	}
	$onInit()
	{
		/* if(!this.$cookie.get('token')){
			window.location.href="http://monitreeapi.monconnect.com/adminpanel/dashboard.php";
			return false;
		} */
		$(".navbar-nav > li > a").click(function(){
			$(".navbar-nav > li").children().removeClass('active');
			$(this).addClass('active');
			$("#toggle-nav-body").addClass('hide-on-mob');
		});
		
		$("#toggle-nav").click(function(){
			$("#toggle-nav-body").toggleClass('hide-on-mob');
		});
		this.userinfo=JSON.parse(window.sessionStorage["userInfo"]);
		this.pieDataset=[];
		this.pieLabel=[];
		this.load_user_portfolio();
		this.investFeed=[];
		this.investAmount=0;	
		this.showLoginBtn=true;
		this.showTradeBtn=false;
		this.showCreditUpstoxBtn=false;
	}
	load_user_portfolio()
	{
		this.$http.get("./api/load_portfolio/"+JSON.stringify([this.userinfo.id,this.userinfo.client_id])).then(response=>{
			if(response.status==200)
			{
				if(response.data.status=="success")
				{
					if(response.data.data!=null) {
						this.investFeed=response.data.data[0];
						this.investAmount=response.data.data[1].amount;
						this.balanceAmount=response.data.data[1].upstoxbalance;
						
						/* this.showTradeBtn=true;
						this.makeTradebtn();
						
						return; */
						if(response.data.data[2].access_token=="" || response.data.data[2].access_token==null)
						{
							this.showLoginBtn=true;
							this.showTradeBtn=false;
							this.showCreditUpstoxBtn=false;
						}
						else
						{
							this.showLoginBtn=false;
							var a=(this.investAmount*2)/100;
							if((this.investAmount-a)>=this.balanceAmount)
							{
								this.showLoginBtn=false;
								this.showTradeBtn=false;
								this.showCreditUpstoxBtn=true;
								$('#modal5').modal();
								$("#modal5").modal('open');
							}
							else
							{
								this.showLoginBtn=false;
								this.showTradeBtn=true;
								this.showCreditUpstoxBtn=false;
								this.makeTradebtn();
								
							}
						}
						for(var i in response.data.data[3])
						{
							this.pieDataset.push(response.data.data[3][i]['total_percent']);
							this.pieLabel.push(response.data.data[3][i]['industry']);
						}
						this.load_jquery(this.pieDataset,this.pieLabel);
					}
				}
			}
		}).catch(error=>{
			M.toast({html: '<span class="red-text"><b>Error:</b>Internal server error</span>'});
		});
	}
	load_jquery(dataSet,labls)
	{
		var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		
		var ctx = document.getElementById('canvas').getContext('2d');
			
		var gradient = ctx.createLinearGradient(0,0,0,300);
						gradient.addColorStop(0, 'rgba(254,93,189,1)');   
						gradient.addColorStop(1, 'rgba(255,255,255,0.1)');
		var gradient1 = ctx.createLinearGradient(0,0,0,300);
						gradient1.addColorStop(0, 'rgba(14,195,212,1)');   
						gradient1.addColorStop(1, 'rgba(255,255,255,0.1)');
		
		var config = {
			type: 'line',
			data: {
				labels: ['31-Jan-18','28-Feb-18','28-Mar-18','30-Apr-18','31-May-18','29-Jun-18','31-Jul-18','31-Aug-18','06-Sep-18'],
				datasets: [{
					label: 'My First dataset',
					fill: true,
					backgroundColor: gradient,
					borderColor: 'rgb(254,93,189)',
					data: [0.05,0.04,0.07,0.24,0.24,0.12,0.18,0.27,0.22],
					pointRadius:5,
					pointBackgroundColor:'#FFFFFF',
					pointBorderWidth:2,
					borderWidth:2,
				},
				{
					label: 'My Second dataset',
					fill: true,
					backgroundColor: gradient1,
					borderColor: 'rgb(14,195,212)',
					data: [0.01,-0.03,-0.07,-0.01,-0.02,-0.03,0.02,0.06,0.04],
					pointRadius:5,
					pointBackgroundColor:'#FFFFFF',
					pointBorderWidth:2,
					borderWidth:2,
				}]
			},
			options: {
					responsive: true,
					title:false,
					tooltips: {
						mode: 'index',
						intersect: false,
					},
					legend:false,
					hover: {
						mode: 'nearest',
						intersect: true
					},
					scales: {
						xAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: ''
							},
							gridLines:false,
							ticks:{padding:10,fontColor:'#000000'}
						}],
						yAxes: [{
							display: true,
							scaleLabel: {
								display: true,
								labelString: ''
							},
							gridLines:false,
							ticks:{padding:10,fontColor:'#000000'}
						}]
					},
					layout: {
						padding: {
							left: -20,
							right: -20,
							top: 0,
							bottom: 0
						}
					}
				}
		};
		window.myLine = new Chart(ctx, config);
		
		
		var config1 = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: dataSet,
					backgroundColor: ["#fdf7db","#60c3a6","#faec38","#83d5f7","#f9a62a","#664499","#994433","#666666","#ffff11","#ff4499","#cc00dd","#ccff88","#992266"],
					label: 'Dataset 1'
				}],
				labels: labls
			},
			options: {
				responsive: false,
				cutoutPercentage: 40,
				legend: false,
				legendCallback: function(chart) {
					var text = [];
					text.push('<ul class="' + chart.id + '-legend">');
					for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
					text.push('<li style="padding: 3px 0;"><span style="color:' + chart.data.datasets[0].backgroundColor[i] + ';font-size:14px;font-weight:bold;">');
					if (chart.data.labels[i]) {
						text.push(chart.data.labels[i]);
					}
					text.push('</span></li>');
					}
					text.push('</ul>');
					return text.join("");
				},
				title: {
					display: true,
					text: ''
				},
				animation: {
					animateScale: true,
					animateRotate: true
				},
				elements: {
					arc: {
						borderWidth: 0
					}
				}
			}
		};
		var draw = Chart.controllers.doughnut.prototype.draw;
					Chart.controllers.doughnut = Chart.controllers.doughnut.extend({
					draw: function() {
						draw.apply(this, arguments);
						let ctx = this.chart.chart.ctx;
						let _fill = ctx.fill;
						ctx.fill = function() {
							ctx.save();
							ctx.shadowColor = 'RGBA(0,0,0,0.5)';
							ctx.shadowBlur = 5;
							ctx.shadowOffsetX = 3;
							ctx.shadowOffsetY = 0;
							_fill.apply(this, arguments)
							ctx.restore();
						}
					}
				});
		
		var ctxx = document.getElementById('chart-area').getContext('2d');
		
		window.myDoughnut = new Chart(ctxx, config1);
		$("#chartjs-legend").html(window.myDoughnut.generateLegend());
	}
	loginToUpstox()
	{
		var body='c_id='+this.userinfo.client_id+'&u_id='+this.userinfo.id;
		window.location.href="http://monitreeapi.monconnect.com/crossd.php?c_id="+this.userinfo.client_id+"&u_id="+this.userinfo.id;
		this.$http({
				method:'post',
				url:'https://jarvisinvest.com/crossd.php',
				data:body,
				headers:{'Content-Type':'application/x-www-form-urlencoded'}
			})
			.then(response=>{
		});
	}
	makeTradebtn()
	{
		var upstoxTradesConfig1='';
		let tradeIds=[];
		var body={id:this.userinfo.client_id};
		this.$http.post("./api/gettradedata",body).then(response=>{
			if(response.data.status=="success")
			{
				var record=response.data.data;
				var buttonname=record[0]["buttonname"];
				for(var j in record)
				{
					tradeIds.push(record[j]['id']);
				}
				$("#_norm").val(JSON.stringify(tradeIds));
				$(".branded-btn").html('<upstox-tradebutton button-name="'+buttonname+'" class="pf-btn gradient" style="display:inline;padding: 14px 90px;" onclick="update('+this.userinfo.client_id+');">TRADE</upstox-tradebutton>');
				UpstoxButton.createButton(buttonname, 'tradebutton');
				for(var i in record)
				{
					upstoxTradesConfig1={"symbol": record[i]['symbol'],"exchange": record[i]['exchange'],"series": "EQ","quantity": record[i]['quantity'],"price":record[i]['price'],"side": record[i]['side'],"complexity": record[i]['complexity'],"orderType": record[i]['order_type'],"position": record[i]['position'],"tif": record[i]['tif']};
					UpstoxButton.addOrderToButton(buttonname, upstoxTradesConfig1);
				
				}
			}
			else if(response.data.status=="error")
			{
				this.showTradeBtn=false;
			}		
		}).catch(error=>{
			M.toast({html: '<span class="red-text"><b>Error:</b> Internal server error</span>'});
		});
	}
}
angular.module("jarvisApp").component('portfolio',{
	templateUrl:'./client/main/portfolio.html',
	controller:PortfolioController
})
})();