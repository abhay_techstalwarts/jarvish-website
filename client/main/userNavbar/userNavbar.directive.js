'use strict';

angular.module('jarvisApp')
  .directive('userNavbar', () => ({
    templateUrl: './client/main/userNavbar/userNavbar.html',
    restrict: 'E',
    controller: 'userNavbarController',
	controllerAs: 'nav'
  }));
