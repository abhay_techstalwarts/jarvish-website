'use strict';

(function() {

    class PaymentDoneController {

        constructor($http, $scope,$state,$rootScope,$cookies) {
            this.$http = $http;
			this.$state = $state;
			this.$rootScope=$rootScope;
			this.$cookies=$cookies;
        }
        
        $onInit() {
			
        }
    }
    angular.module('jarvisApp')
        .component('paymentdone', {
            controller: PaymentDoneController,
			templateUrl:'./client/main/paymentdone.html',
        });
})();