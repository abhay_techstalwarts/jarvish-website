'use strict';

(function() {

    class LogoutController {

        constructor($http, $scope,$state,$rootScope,$cookies) {
            this.$http = $http;
			this.$state = $state;
			this.$rootScope=$rootScope;
			this.$cookies=$cookies;
        }
        
        $onInit() {
			this.$cookies.remove('token');
			sessionStorage.clear();
			this.$state.go('/');
        }
    }
    angular.module('jarvisApp')
        .component('logout', {
            controller: LogoutController
        });
})();