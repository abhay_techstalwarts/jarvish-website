'use strict';

angular.module('jarvisApp')
  .directive('navbar', () => ({
    templateUrl: './client/main/navbar/navbar.html',
    restrict: 'E',
    controller: 'NavbarController',
	controllerAs: 'nav'
  }));
