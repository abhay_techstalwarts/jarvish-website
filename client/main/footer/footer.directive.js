'use strict';

angular.module('jarvisApp')
  .directive('uiFooter', () => ({
    templateUrl: './client/main/footer/footer.html',
    restrict: 'E',
    controller: 'FooterController',
	controllerAs: 'foot'
  }));
