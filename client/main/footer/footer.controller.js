'use strict';

(function() {

    class FooterController {

        constructor($http, $scope,$state) {
            this.$http = $http;
            this.$state = $state;
        }
		$onInit(){
			this.contact={name:null,email:null,mobile:null,subject:null,message:null};
			this.newemail=null;
        }		
        submitContact(contactForm){
			if(!contactForm.$invalid) {
				var body = this.contact;
				this.myPromise=this.$http.POST('./api/contact',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
						}
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> '+error+'</span>'});
					this.myPromise=false;
				});
			}
		}
		submitNewletter(newletterForm)
		{
			if(!newletterForm.$invalid) {
				var body = {email:this.newemail};
				this.myPromise=this.$http.post('./api/newletter',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
						}
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> '+error+'</span>'});
					this.myPromise=false;
				});
			}
		}
    }
    angular.module('jarvisApp').controller('FooterController', FooterController);
})();