(function(){
class createPortfolioController
{
	constructor($scope,$http,$state,$rootScope,$cookies,$timeout)
	{
		this.$http = $http;
		this.$state = $state;
		this.$rootScope=$rootScope;
		this.$cookies=$cookies;
		this.$timeout=$timeout;
		this.$scope=$scope;
	}
	$onInit()
	{
		$(".navbar-nav > li > a").click(function(){
			$(".navbar-nav > li").children().removeClass('active');
			$(this).addClass('active');
			$("#toggle-nav-body").addClass('hide-on-mob');
		});
		
		$("#toggle-nav").click(function(){
			$("#toggle-nav-body").toggleClass('hide-on-mob');
		});
		this.advSelection=false;
		this.showAccountBtn=true;
		this.$rootScope.userinfo=JSON.parse(window.sessionStorage["userInfo"]);
		this.amount=(this.$cookies.get('investAmount')!=undefined)?this.$cookies.get('investAmount'):null;
		this.tenure=(this.$cookies.get('investYear')!=undefined)?this.$cookies.get('investYear'):null;
		this.pointer=(window.innerWidth<700)?false:true;
		this.priceSlider = {
			value: 0,
			options: {
				ceil: 6,
				floor: 0,
				showSelectionBar: true,
				showTicks: this.pointer,
				hidePointerLabels: true,
				hideLimitLabels: true,
				stepsArray:[{value:1,legend:'Risk averse'},{value:2,legend:'Conservative'},{value:3,legend:'Moderate'},{value:4,legend:'Moderately Aggresive'},{value:5,legend:'Aggresive'},{value:6,legend:'Very Aggresive'},]
			}
		};
		this.feed=[];
		this.feed1=[];
		this.myFeed=[];
		this.progressAvg=0;
		$('#modal3').modal({dismissible:false});
		$('#modal5').modal({dismissible:false});
		this.prefer={large_cap:false,mid_cap:false,small_cap:false,all_cap:true};
		$(".dropdown-trigger").dropdown();
		this.cidtxt=false;
		this.c_pass=null;
		this.checkClientIdExist();
		//this.getFeed();
	}
	includeAll(type)
	{
		if(type==1)
		{
			for(var i in this.feed)
			{
				this.feed[i].include=true;
			}
		}
		else if(type==2)
		{
			for(var i in this.feed1)
			{
				this.feed1[i].include=true;
			}
		}
	}
	getFeed()
	{
		//code for checkbox (when user select large,mid or small cap then no preference should be checked)
		
		if(this.prefer.large_cap===true || this.prefer.mid_cap===true ||this.prefer.small_cap===true){
			this.prefer.all_cap=false;
		} else {
			this.prefer.all_cap=true;
		}
		this.totalFeedsum=0;
		this.$http.get("./api/feed/"+JSON.stringify(this.prefer)).then(response=>{
			this.feed=response.data;
			this.feed1=response.data;
			var k=0;
			for(var j in response.data)
			{
				k++;
				this.totalFeedsum=parseInt(this.totalFeedsum)+parseInt(response.data[j]['allocation']);
				
			}
			this.progressAvg=Math.round(this.totalFeedsum/k);
		});
	}
	/*getFeed()
	{
		if(this.prefer.large_cap===true || this.prefer.mid_cap===true ||this.prefer.small_cap===true){
			this.prefer.all_cap=false;
		} else {
			this.prefer.all_cap=true;
		}
		var cap=[];
		if(this.prefer.large_cap===true) {cap.push('Large Cap');}
		if(this.prefer.mid_cap===true) {cap.push('Mid Cap');}
		if(this.prefer.small_cap===true) {cap.push('Small Cap');}
		this.totalFeedsum=0;
		var body={MarketCap:cap,MarketExcluded:[],RiskParams:this.$rootScope.userinfo.risk_factor,PortFolioValue:this.amount};
		this.$http.post("https://core.jarvisinvest.com:8000/portfolio",body).then(response=>{
			this.feed=this.format_data(response.data);
			this.feed1=this.feed;
			var k=0;
			for(var j in response.data)
			{
				k++;
				this.totalFeedsum=parseInt(this.totalFeedsum)+parseInt(response.data[j]['allocation']);
				
			}
			this.progressAvg=Math.round(this.totalFeedsum/k);
		});
	}*/
	format_data(data)
	{
		var a=[];
		for(var i in data){
			a.push({sector:data[i]['Portfolio_Symbol'],allocation:data[i]['Portfolio_Weight'],asTotal:data[i]['Portfolio_Weight'],include:true});
		}
		return a;
	}
	makeProtfolio(type)
	{
		if(this.tenure!='' || this.tenure>0)
		{
			if(this.amount!="" && this.amount>=50000)
			{
				if(type==1)
				{
					for(var i in this.feed)
					{
						if(this.feed[i].include==1)
						{
							this.myFeed.push({sector:this.feed[i]['sector'],allocation:this.feed[i]['allocation'],total:this.feed[i]['total'],user_id:this.$rootScope.userinfo.id});
						}
					}
				}
				else if(type==2)
				{
					for(var i in this.feed1)
					{
						if(this.feed1[i].include==1)
						{
							this.myFeed.push({sector:this.feed1[i]['sector'],allocation:this.feed1[i]['allocation'],total:this.feed1[i]['total'],user_id:this.$rootScope.userinfo.id});
						}
					}
				}
				if(this.myFeed.length>0)
				{
					$("#modal5").modal('open');
				}
				else
				{
					M.toast({html: '<span class="red-text"><b>Error:</b> Include the sectors in advance selection</span>'});
				}
				
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Minimum investment amount is Rs. 50,000</span>'});
			}
		}
		else
		{
			M.toast({html: '<span class="red-text"><b>Error:</b> Year should be greater than zero</span>'});
		}
	}
	submitPortfolio(){
		if(this.tenure!='' || this.tenure>0)
		{
			if(this.amount!="" && this.amount>=50000)
			{
				if(this.myFeed.length>0)
				{
					this.$http.post("./api/makeportfolio",{data:this.myFeed,prefer:this.prefer,amount:this.amount,tenure:this.tenure,user_id:this.$rootScope.userinfo.id}).then(response=>{
					if(response.status==200)
					{  
						if(response.data.status=="success")
						{
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
							
							//window.open("https://upstox.com/open-account/?f=mwZR",'height=500,width=500');
							//setTimeout(function(){window.location.reload();},500);
							$("#modal5").modal('close');
							$('#modal4').modal({
								dismissible:false
							});
							$("#modal4").modal('open');
						}
						else
						{
							M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
						}
					}
					}).catch(error=>{
						M.toast({html: '<span class="red-text"><b>Error:</b> '+error.data.message+'</span>'});
					});
				}
				else
				{
					M.toast({html: '<span class="red-text"><b>Error:</b> Include the sectors in advance selection</span>'});
				}
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Minimum investment amount is Rs. 50,000</span>'});
			}
		}
		else
		{
			M.toast({html: '<span class="red-text"><b>Error:</b> Year should be greater than zero</span>'});
		}
	}
	checkClientIdExist()
	{
		this.$http.get("./api/checkClientIdExist/"+this.$rootScope.userinfo.id).then(response=>{
			if(response.status==200) 
			{
				
				if(response.data.count==1) // client id blank
				{
					if(response.data.count1>0) // portfolio is not blank
					{
						this.showAccountBtn=false;// if client is blank
						$('#modal4').modal({
							dismissible:false
						});
						$("#modal4").modal('open');
					}
					else // portfolio blank
					{
						if(this.$rootScope.userinfo.risk_factor==0 || this.$rootScope.userinfo.risk_factor== null || this.$rootScope.userinfo.risk_factor=="")
						{
							$("#modal3").modal('open');
							setTimeout(()=>{this.$scope.$broadcast('rzSliderForceRender');},200);
						}		
					}
				}
				else // client id is not blank
				{
					if(response.data.count1>0) // portfolio is not blank
					{
						window.location.href="portfolio";
						//this.$state.go('portfolio');
					}
					else // portfolio blank
					{
						if(this.$rootScope.userinfo.risk_factor==0)
						{
							$("#modal3").modal('open');
							setTimeout(()=>{this.$scope.$broadcast('rzSliderForceRender');},200);
						}	
					}
				}
			}
		});
	}
	openTermsModal(){
		$("#modal5").modal('open');
	}
	openAccount()
	{
		//$("#modal5").modal('close');
		window.open("https://upstox.com/open-account/?f=mwZR",'height=500,width=500');
	}
	closeModal(){
		$('#modal5').modal('close');
	}
	saveRisk()
	{
		this.$http.post("./api/saverisk",{value:this.priceSlider.value,user_id:this.$rootScope.userinfo.id})
		.then(response=>{
			if(response.status==200)
			{  
				if(response.data.status=="success")
				{
					this.$rootScope.userinfo['risk_factor']=response.data.data;
					window.sessionStorage["userInfo"] = JSON.stringify(this.$rootScope.userinfo);
					M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
					$("#modal3").modal('close');
				}
				else
				{
					M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
				}
			}
		}).catch(error=>{
				M.toast({html: '<span class="red-text"><b>Error:</b> Internal Server Error</span>'});
		});
	}
	goUpstox(){
		/* copied code form portfolio controller for hitting upstox login api */
		var body='c_id='+this.$rootScope.userinfo.client_id+'&u_id='+this.$rootScope.userinfo.id;
		window.location.href="http://monitreeapi.monconnect.com/crossd.php?c_id="+this.$rootScope.userinfo.client_id+"&u_id="+this.$rootScope.userinfo.id;
		this.$http({
				method:'post',
				url:'https://jarvisinvest.com/crossd.php',
				data:body,
				headers:{'Content-Type':'application/x-www-form-urlencoded'}
			})
			.then(response=>{
			console.log(response.data);
		});
	}
	goPortfolio()
	{
		this.$http.get("./api/cexist/"+this.$rootScope.userinfo.id).then(response=>{
			if(response.data.status=="success"){
				if(response.data.data==true)
				{
					this.cidtxt=true;
				}
				else
				{
					//window.location.href="https://jarvisinvest.com/portfolio";
					this.goUpstox();
				}
			}
		}).catch(error=>{
			M.toast({html: '<span class="red-text"><b>Error:</b> Internal Server Error</span>'});
		});
	}
	filterValue(event)
	{	
		if(this.tenure!==null) { if(this.tenure.length<2 && this.tenure>0){this.tenure=0+this.tenure}else{this.tenure=this.tenure.replace(/^0+/, '');} }
	}
	submitCid(cidForm)
	{
		if(!cidForm.$invalid) {
			var body = {cid:this.cid,cpass:this.c_pass,uid:this.$rootScope.userinfo.id};
			this.myPromise=this.$http.post('./api/update_cid',body).then((response)=>{
				if(response.status==200){
					if(response.data.status=="success")
					{
						this.$rootScope.userinfo['client_id']=response.data.data;
						window.sessionStorage["userInfo"] = JSON.stringify(this.$rootScope.userinfo);
						this.goUpstox();
					}
					else if(response.data.status=="error")
					{
						M.toast({html: '<span class="red-text"><b>Error:</b> Internal Server Error</span>'});
					}
					
				}
			}).catch(error=> {
				M.toast({html: '<span class="red-text"><b>Error:</b> Upstox ID already exist in database.</span>'});
			});
		}
	}
}
angular.module("jarvisApp").component('createPortfolio',{
	templateUrl:'./client/main/create-portfolio.html',
	controller:createPortfolioController
})
})();