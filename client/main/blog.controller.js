'use strict';

(function() {

    class BlogController {

        constructor($http, $scope,$state,$rootScope,$cookies) {
            this.$http = $http;
			this.$state = $state;
			this.$rootScope=$rootScope;
			this.$cookies=$cookies;
        }
        
        $onInit() {
            $('.modal').modal();
			this.username=null;
			this.password=null;
			this.myPromise=false;
			this.error='';
			this.showOtp=false;
			this.showOauthMob=false;
			this.signupForm=true;
			this.mobile_no=null;
			this.oauth_mob=null;
			this.o_data=null;
			this.loginwindow=true;
			this.forgotWindow=false;
			this.changePasswordWindow=false;
			this.amount=null;
			this.year=null;
			this.accept=false;
			this.acceptoauth=false;
		}
		login(loginForm){
			if(!loginForm.$invalid) {
				var body = {username:this.username,password:this.password};
				this.myPromise=this.$http.post('./api/auth',body).then((response)=>{
					if(response.status==200){
							window.sessionStorage["userInfo"] = JSON.stringify(response.data.userInfo);
							this.$cookies.put('token',response.data.token);
							/*M.toast({html: '<span class="green-text">You have logged in successfully.</span>'});*/
							this.check_portfolio_exist(response.data.userInfo.id);
					}
				}).catch(error=> {
					if(error.status==401)
					{
						M.toast({html: '<span class="red-text"><b>Error:</b> '+error.data.message+'</span>'});
					}
				});
			}
		}
		register(loginForm){
			if(this.accept==false)
			{
				M.toast({html: '<span class="red-text">Please accept Terms & Conditions</span>'});
				return false;
			}
			if(!loginForm.$invalid) {
				var body = {name:this.name,password:this.pwd,email:this.email,mobile:this.mobile,oauth:0,profile_pic:null};
				this.myPromise=this.$http.post('./api/register',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							this.showOtp=true;
							$("#sign-up-panel-1,#sign-up-panel-2").addClass('hidden');
							this.mobile_no=response.data.data.mobile;
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
						}
						return;	
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> Internal Server Error</span>'});
				});
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Fill all form fields</span>'});
			}
		}
		verify(otpForm)
		{
			if(!otpForm.$invalid) {
				var body = {otp:this.otp,mobile:this.mobile_no};
				this.myPromise=this.$http.post('./api/verify',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
							//$("#modal2").modal('close');
							window.sessionStorage["userInfo"] = JSON.stringify(response.data.data.userInfo);
							this.$cookies.put('token',response.data.data.token);
							this.check_portfolio_exist(response.data.data.userInfo.id);
							this.signupForm=true;
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text">'+response.data.message+'</span>'});
						}
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> '+error+'</span>'});
				});
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Fill all form fields</span>'});
			}
		}
		resend_otp(otpForm)
		{
			if(this.mobile_no!=null || this.mobile_no!='') {
				var body = {mobile:this.mobile_no};
				this.myPromise=this.$http.post('./api/resend',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text">'+response.data.message+'</span>'});
						}
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> '+error+'</span>'});
				});
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Please try after some time.</span>'});
			}
		}
		forgot()
		{
			if(this.mobile_id!=null || this.mobile_id!='') {
				var body = {mobile:this.mobile_id};
				this.myPromise=this.$http.post('./api/resend',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							this.forgotWindow=false;
							this.changePasswordWindow=true;
							this.forMobile_no=response.data.data.mobile;
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
						}
						return;	
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> '+error+'</span>'});
				});
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Mobile no is missing.</span>'});
			}
		}
		submitForgot(forOtpForm)
		{
			if(!forOtpForm.$invalid) {
				var body = {otp:this.forotp,mobile:this.forMobile_no,password:this.forPwd};
				this.myPromise=this.$http.post('./api/forget',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							this.loginwindow=true;this.forgotWindow=false;this.changePasswordWindow=false;
							M.toast({html: '<span class="green-text">'+response.data.message+'</span>'});
							this.mobile_id=null;
							this.forMobile_no=null;
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text">'+response.data.message+'</span>'});
						}
					}
				}).catch(error=> {
					M.toast({html: '<span class="red-text"><b>Error:</b> '+error+'</span>'});
				});
			}
			else
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> Fill all form fields</span>'});
			}
		}
		down()
		{
			$('html,body').animate({
				scrollTop: 550},
				'slow');
		}
		totop()
		{
			$('html,body').animate({
				scrollTop: 0},
				'slow');
		}
		FBRegister()
		{
			FB.login((response)=> {
				if (response.authResponse) 
				{
					FB.api('/me?fields=email,name,first_name,last_name,id',(response)=> {
						if(response!='' || response!=null)
						{
							/* chech oauth email id exist on jarvis */
							this.$http.post('./api/email_exist',{email:response.email}).then(res=>{
								if(res.data.status==="success")
								{
									/*login*/
									window.sessionStorage["userInfo"] = JSON.stringify(res.data.data.userInfo);
									this.$cookies.put('token',res.data.data.token);
									this.check_portfolio_exist(res.data.data.userInfo.id);
								}
								else if(res.data.status==="error")
								{
									/*register*/
									this.showOauthMob=true;
									this.signupForm=false;
									this.o_data=JSON.stringify(response);
								}
							});
						}					
					});
		
				} 
				else
				{
					//alert('Authorization failed.');
				}
			},{scope: 'public_profile,email'});
		
		}
		FBLogin()
		{
			FB.getLoginStatus((response)=>{
				if (response.status === 'connected') {
					this.oauthLogin();
				}
				else
				{
					FB.login((response)=> {
						if (response.authResponse) 
						{
							this.oauthLogin();
						}
					});
				}
			});
		}
		oauthLogin()
		{
			FB.api('/me?fields=email',(response)=> {
				/* chech oauth email id exist on jarvis */
					this.$http.post('./api/email_exist',{email:response.email}).then(res=>{
					if(res.data.status==="success")
					{
						/*login*/
						window.sessionStorage["userInfo"] = JSON.stringify(res.data.data.userInfo);
						this.$cookies.put('token',res.data.data.token);
						this.check_portfolio_exist(res.data.data.userInfo.id);
					}
					else if(res.data.status==="error")
					{
						M.toast({html: '<span class="red-text">You have not signed up on Jarvis.</span>'});
						return;
					}
				});
			});
		}
		submit_mobile(mobileForm)
		{
			if(this.acceptoauth==false)
			{
				M.toast({html: '<span class="red-text">Please accept Terms & Conditions</span>'});
				return false;
			}
			if(this.oauth_mob!="" || this.oauth_mob!=null)
			{
				var response=JSON.parse(this.o_data);
				var name=response.first_name+" "+response.last_name;
				var str=response.first_name+""+response.last_name;
				var password=(str.substr(0,5)).toLocaleLowerCase()+"@123";
				var profile_pic="http://graph.facebook.com/" + response.id + "/picture?type=normal";
				var body={name:name,email:response.email,mobile:this.oauth_mob,password:password,oauth:1,profile_pic:profile_pic}
				this.myPromise=this.$http.post('./api/register',body).then((response)=>{
					if(response.status==200){
						if(response.data.status=="success")
						{
							this.showOauthMob=false;
							this.showOtp=true;
							$("#sign-up-panel-1,#sign-up-panel-2").addClass('hidden');
							this.mobile_no=response.data.data.mobile;
						}
						else if(response.data.status=="error")
						{
							M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
						}
						return;	
					}
				});
			}
		}
		check_portfolio_exist(user_id)
		{
			this.$http.get("./api/exist_portfolio/"+user_id).then(response=>{
				if(response.status==200)
				{
					if(response.data.status=="success")
					{
						if(response.data.count>0)
						{
							window.location.href="portfolio";
							//this.$state.go('portfolio', {}, {reload: true});
						}
						else
						{
							window.location.href="create-portfolio";
							//this.$state.go('create-portfolio', {}, {reload: true});
						}
					}
				}
			}).catch(error=>{
				M.toast({html: '<span class="red-text"><b>Error:</b> '+error.data.message+'</span>'});
			});
		}
		invest()
		{
			
			if(this.$cookies.get('token'))
			{
				var a=JSON.parse(window.sessionStorage["userInfo"]);
				this.check_portfolio_exist(a.id);
			}
			else
			{
				this.$cookies.put('investYear',this.year);
				this.$cookies.put('investAmount',this.amount);
				$("#modal1").modal('open');
			}
		}
		filterValue(event)
		{	
			if(this.year.length<2 && this.year>0){this.year=0+this.year}else{this.year=this.year.replace(/^0+/, '');}
		}
		goTerms()
		{
			window.location.href="terms-and-conditions";
		}
    }
    angular.module('jarvisApp')
        .component('blog', {
            templateUrl: './client/main/blog.html',
            controller: BlogController
        });
})();