(function(){
class profileController
{
	constructor($scope,$http,$state,$rootScope,$cookies,$timeout)
	{
		this.$http = $http;
		this.$state = $state;
		this.$rootScope=$rootScope;
		this.$cookies=$cookies;
		this.$timeout=$timeout;
	}
	$onInit()
	{
		this.$rootScope.userInfo=JSON.parse(window.sessionStorage["userInfo"]);
		$(".datepicker").datetimepicker({
			timepicker:false,
			format:'d-m-Y',
			maxDate:'0',
			yearStart:1950
		});
		$("#pro_upload").click(function(){
			$("#file").trigger('click');
			return false;
		});
		$('.tooltipped').tooltip();
		var file_input=document.getElementById('file');
		file_input.addEventListener('change',(e)=> {
			this.readImage(file_input).done((base64Data)=>{
				this.$rootScope.userinfo.profile_pic=base64Data;
			});
			var data = new FormData();
			data.append('image', $("#file")[0].files[0]);
			data.append('id', this.$rootScope.userInfo.id);
			$.ajax('./api/update_pic', {
				method: 'POST',
				data: data,
				contentType: false,
				processData: false,
				enctype:'multipart/form-data',
				headers:undefined,
				success: function (html) {
					var b=JSON.parse(window.sessionStorage['userInfo']);
					b.profile_pic=html.data;
					window.sessionStorage['userInfo']=JSON.stringify(b);
				}
			});
		});

		this.personal={id:this.$rootScope.userInfo.id,name:null,email:null,mobile:null,income:null,designation:null,education:null,account:null,father:null,father_age:null,mother:null,mother_age:null,spouse:null,spouse_age:null,childs:[],address:null,bank_name:null};
		this.father_year=0;
		this.mother_year=0;
		this.spouse_year=0;
		this.child_year=[];
		this.load_data();
	}
	readImage(inputElement) {
		var deferred = $.Deferred();
	
		var files = inputElement.files;
		if (files && files[0]) {
			var fr= new FileReader();
			fr.onload = function(e) {
				deferred.resolve(e.target.result);
			};
			fr.readAsDataURL( files[0] );
		} else {
			deferred.resolve(undefined);
		}
	
		return deferred.promise();
	}
	moveTo(menu){
		this.$state.go(menu);
	}
	load_data(){
		this.$http.get('./api/get_profile/'+this.$rootScope.userInfo.id)
		.then(response=>{
			if(response.data.status=="success")
			{
				this.personal=response.data.data;
				if(this.personal.childs==null){
					this.personal.childs=JSON.stringify([{child:null,child_age:null}]);
					this.child_year=[0];
				}
				this.personal.childs=JSON.parse(this.personal.childs);
				this.calculateAge(1,0);this.calculateAge(2,0);this.calculateAge(3,0);
				setTimeout(()=>{$(".input-type label").addClass('active');this.calculateAge(1,0);this.calculateAge(2,0);this.calculateAge(3,0);},500);
			}
			else if(response.data.status=="error")
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
			}
			
		});
	}
	submitProfile(){
		this.$http.post('./api/submit_profile',this.personal)
		.then(response=>{
			if(response.data.status=="success")
			{
				M.toast({html: '<span class="green-text"><b>Success:</b> '+response.data.message+'</span>'});
			}
			else if(response.data.status=="error")
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
			}
		});
	}
	calculateAge(type,index) 
	{
		var birthday=(type==1)?this.personal.father_age:(type==2)?this.personal.mother_age:(type==3)?this.personal.spouse_age:(type==4)?this.personal.childs[index]['child_age']:'00:00:0000';
		if(birthday==null || birthday=='')
		{
			birthday='00:00:0000';
		}
		var splitB=birthday.split('-');
		var thisYear=new Date().getFullYear();
		var birthYear=new Date(splitB[2]+'-'+splitB[1]+'-'+splitB[0]).getFullYear();
		var year_age=thisYear-birthYear;
        if (isNaN(year_age))
		{
			return 0;
		}
		else
		{
			if(type==1){
				this.father_year=year_age;
			}
			else if(type==2){
				this.mother_year=year_age;
			}
			else if(type==3){
				this.spouse_year=year_age;
			}
			else if(type==4){
				this.child_year[index]=year_age;
			}
			else
			{
				return 0;
			}
		}
	}
	addChild()
	{
		var child={child:null,child_age:null};
		this.personal.childs.push(child);
		this.child_year.push(0);
	}
	initilizeDatePicker(){
		$(".datepicker").datetimepicker({'timepicker':false,'format':'d-m-Y'});
	}
	childClose(index)
	{
		this.personal.childs.splice(index);
		this.child_year[index]=0;
	}
	
}
angular.module("jarvisApp").component('profile',{
	templateUrl:'./client/main/profile.html',
	controller:profileController
})
})();