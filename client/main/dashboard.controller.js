(function(){
class dashboardController
{
	constructor($scope,$http,$state,$rootScope,$cookies,$timeout)
	{
		this.$http = $http;
		this.$state = $state;
		this.$rootScope=$rootScope;
		this.$cookies=$cookies;
		this.$timeout=$timeout;
	}
	$onInit()
	{
		this.$rootScope.userInfo=JSON.parse(window.sessionStorage["userInfo"]);
		this.personal=[];
		this.chart();
		this.invest_amount=0;
		this.current_amount=0;
		this.badge=false;
		this.port_allocation=[];
		this.load_data();
		this.load_dash();
	}
	chart()
	{
		var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var ctx = document.getElementById('canvas').getContext('2d');
			
		var gradient = ctx.createLinearGradient(0,0,0,340);
						gradient.addColorStop(0, 'rgba(254,93,189,1)');   
						gradient.addColorStop(1, 'rgba(255,255,255,0.1)');
		var gradient1 = ctx.createLinearGradient(0,0,0,340);
						gradient1.addColorStop(0, 'rgba(14,195,212,1)');   
						gradient1.addColorStop(1, 'rgba(255,255,255,0.1)');
	
		var config = {
			type: 'line',
			data: {
				labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
				datasets: [{
					label: 'My First dataset',
					fill: true,
					backgroundColor: gradient,
					borderColor: 'rgb(254,93,189)',
					data: [45,40,55,25,45,30,60,55,20,55,60,55],
					pointRadius:5,
					pointBackgroundColor:'#FFFFFF',
					pointBorderWidth:1,
					borderWidth:1,
				},
				{
					label: 'My Second dataset',
					fill: true,
					backgroundColor: gradient1,
					borderColor: 'rgb(14,195,212)',
					data: [65,60,30,45,10,60,65,30,40,25,40,65],
					pointRadius:5,
					pointBackgroundColor:'#FFFFFF',
					pointBorderWidth:1,
					borderWidth:1,
				}]
			},
			options: {
				responsive: true,
				title:false,
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				legend:false,
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: ''
						},
						gridLines:false,
						ticks:{padding:10,fontColor:'#000000'}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: ''
						},
						gridLines:false,
						ticks:{padding:10,fontColor:'#000000'}
					}]
				},
				layout: {
					padding: {
						left: -20,
						right: -20,
						top: 0,
						bottom: 0
					}
				}
			}
		};
		window.myLine = new Chart(ctx, config);
	}
	moveTo(menu)
	{
		this.$state.go(menu);
	}
	load_data(){
		this.$http.get('./api/get_profile/'+this.$rootScope.userInfo.id)
		.then(response=>{
			if(response.data.status=="success")
			{
				this.personal=response.data.data;
				$(".input-type label").addClass('active');
			}
			else if(response.data.status=="error")
			{
				M.toast({html: '<span class="red-text"><b>Error:</b> '+response.data.message+'</span>'});
			}
			
		});
	}
	load_dash(){
		this.load_bse_feed();
		this.$http.get('./api/load_dash/'+this.$rootScope.userInfo.id+'/'+this.$rootScope.userInfo.client_id)
		.then(response=>{
			if(response.data.status=="success")
			{
				this.invest_amount=response.data.data['invest'];
				this.current_amount=response.data.data['ltp_amt'];
				this.port_allocation=response.data.data['allocation'];
				this.per_monthly=response.data.data['performance'][0]['monthly'];
				this.per_quarterly=response.data.data['performance'][0]['quarterly'];
				this.per_yearly=response.data.data['performance'][0]['yearly'];
				this.badge=true;
			}		
			else
			{
				this.badge=false;
			}
		});
	}
	load_bse_feed(){
		this.$http.get('./api/load_bse/'+this.$rootScope.userInfo.id+'/'+this.$rootScope.userInfo.client_id)
		.then(response=>{
			if(response.data.status=="success"){
				this.bse_per_monthly=response.data.data['performance'][0]['monthly'];
				this.bse_per_quarterly=response.data.data['performance'][0]['quarterly'];
				this.bse_per_yearly=response.data.data['performance'][0]['yearly'];
			}
		});
	}
	
	
}
angular.module("jarvisApp").component('dashboard',{
	templateUrl:'./client/main/dashboard.html',
	controller:dashboardController
})
})();