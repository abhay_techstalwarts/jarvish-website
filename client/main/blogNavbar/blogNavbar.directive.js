'use strict';

angular.module('jarvisApp')
  .directive('blogNavbar', () => ({
    templateUrl: './client/main/blogNavbar/blogNavbar.html',
    restrict: 'E',
    controller: 'blogNavbarController',
	controllerAs: 'nav'
  }));
