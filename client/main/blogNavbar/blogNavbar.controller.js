'use strict';

(function() {

    class blogNavbarController {

        constructor($http, $scope,$rootScope,$state,$cookies) {
            this.$http = $http;
            this.$state = $state;
			this.$cookies=$cookies;
			this.$rootScope=$rootScope;
        }
		$onInit()
		{
			this.$rootScope.userinfo=(window.sessionStorage["userInfo"])?JSON.parse(window.sessionStorage["userInfo"]):'';
			$(".navbar-nav > li > a").click(function(){
				$(".navbar-nav > li").children().removeClass('active');
				$(this).addClass('active');
				$("#toggle-nav-body").addClass('hide-on-mob');
			});
			
			$("#toggle-nav").click(function(){
				$("#toggle-nav-body").toggleClass('hide-on-mob');
			});
			$(window).scroll(function() {
				if ($(this).scrollTop() > 150){
					$(".arrow_up").removeClass('hidden');
				}
				else
				{
					$(".arrow_up").addClass('hidden');
				}
			});
			$("#next").click(function(){
				$("#sign-up-panel-1").addClass('hidden');
				$("#sign-up-panel-2").removeClass('hidden');
			});
			$("#back").click(function(){
				$("#sign-up-panel-1").removeClass('hidden');
				$("#sign-up-panel-2").addClass('hidden');
			});
			/* this.$rootScope.userinfo=window.sessionStorage["userInfo"]; */
			setTimeout(function(){$('.dropdown-trigger').dropdown(/* {hover:true} */);},0);
			$('.tooltipped').tooltip();
		}
		check_portfolio_exist()
		{
			this.$http.get("./api/exist_portfolio/"+this.$rootScope.userinfo.id).then(response=>{
				if(response.status==200)
				{
					if(response.data.status=="success")
					{
						if(response.data.count>0)
						{
							window.location.href="portfolio";
							//this.$state.go('portfolio', {}, {reload: true});
						}
						else
						{
							window.location.href="create-portfolio";
							//this.$state.go('create-portfolio', {}, {reload: true});
						}
					}
				}
			}).catch(error=>{
				M.toast({html: '<span class="red-text"><b>Error:</b> '+error.data.message+'</span>'});
			});
		}

        
    }
    angular.module('jarvisApp').controller('blogNavbarController', blogNavbarController);
})();