$(function(){
	let d = localStorage.getItem('d');
	let a = localStorage.getItem('a');
	if(d != null && a != null){
		setTimeout(function(){scrolls(d,a)}, 1200);
	}
});
$(function(){
	// vars for clients list carousel
	 // http://stackoverflow.com/questions/6759494/jquery-function-definition-in-a-carousel-script
	 var $clientcarousel = $('#clients-list');
	 var clients = $clientcarousel.children().length;
	 var clientwidth = (clients * 220); // 140px width for each client item 
	 $clientcarousel.css('width',clientwidth);
	 
	 var rotating = true;
	 var clientspeed = 0;
	 var seeclients = setInterval(rotateClients, clientspeed);
	 
	 $(document).on({
	   mouseenter: function(){
		 rotating = false; // turn off rotation when hovering
	   },
	   mouseleave: function(){
		 rotating = true;
	   }
	 }, '#clients');
	 
	 function rotateClients() {
	   if(rotating != false) {
		 var $first = $('#clients-list li:first');
		 $first.animate({ 'margin-left': '-220px' }, 2000, "linear", function() {
		   $first.remove().css({ 'margin-left': '0px' });
		   $('#clients-list li:last').after($first);
		 });
	   }
	 }
   });
   

function scrolls(d,a)
{

	
	$('.'+d).addClass('active');
	if(d == "blog" || d == "price_1" || d == "terms" || d=="privacy" || d == "disclaimer" || d == "faq")
	{
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
		$("html, body").animate({scrollTop:$('#'+d).offset().top-a},1200);
		localStorage.clear();
		setTimeout(function(){$('.'+d).addClass('active');
	},500);
	
}

function setLS(d,a){

	localStorage.setItem('d',d);
	localStorage.setItem('a',a);

	window.location = '/';

}

function getCookie(name){
    var pattern = RegExp(name + "=.[^;]*")
    matched = document.cookie.match(pattern)
    if(matched){
        var cookie = matched[0].split('=')
        return cookie[1]
    }
    return false
}
function update(cid){
	var items=$("#_norm").val();
	$.ajax({
		type:'post',
		url:'https://jarvisinvest.com/api/trade',
		data:'items='+items+'&client_id='+cid,
		dataType:'JSON',
		headers:{'Authorization':'Bearer '+getCookie('token')},
		success:function(res){
			$(".modal-overlay").css('z-index','9999 !important');
			$('#modal10').modal({dismissible:false});
			$("#modal10").modal('open');
			return false;
		}
	})
}

		
$(window).load(function() {
	var s_val = sessionStorage.getItem("visitsite");
	if(s_val = "null"){
	sessionStorage.setItem("visitsite", "yes");
	console.log(s_val);
	$('#mdopen').click();
	var counter = 10;
	
	var interval = setInterval(function() {
		counter--;
		console.log(counter);
		document.getElementById("counttext").innerHTML = counter;
		if (counter == 0) {
			clearInterval(interval);
			$('#mdclose').click();

		}
	}, 1000);
	"use strict";

	var dateObj = new Date();
	dateObj.setDate(0);
	var  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	var month = dateObj.getMonth();
	var monthName=months[month];
    var day = dateObj.getDate();
    var year = dateObj.getFullYear();
    newdate =  day + " " + monthName +  " " + year;
	// $('#q3 input').val(newdate);
	document.getElementById("lastMonthPreDate").innerHTML = newdate;
}

productScroll();
window.requestAnimationFrame = (function(){
	return  window.requestAnimationFrame       ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			function( callback ){
			  window.setTimeout(callback, 1000 / 30);
			};
  })();
  
  var speed = 10000;
  (function currencySlide(){
	  var currencyPairWidth = $('.slideItem:first-child').outerWidth();
	  $(".slideContainer").animate({marginLeft:-currencyPairWidth},speed, 'linear', function(){
				  $(this).css({marginLeft:0}).find("li:last").after($(this).find("li:first"));
		  });
		  requestAnimationFrame(currencySlide);
  })();
	});
	// polyfill



function productScroll() {
  let slider = document.getElementById("slider");
  let next = document.getElementsByClassName("pro-next");
  let prev = document.getElementsByClassName("pro-prev");
  let slide = document.getElementById("slide");
  let item = document.getElementById("slide");

  for (let i = 0; i < next.length; i++) {
    //refer elements by class name

    let position = 0; //slider postion

    prev[i].addEventListener("click", function() {
      //click previos button
      if (position > 0) {
        //avoid slide left beyond the first item
        position -= 1;
        translateX(position); //translate items
      }
    });

    next[i].addEventListener("click", function() {
      if (position >= 0 && position < hiddenItems()) {
        //avoid slide right beyond the last item
        position += 1;
        translateX(position); //translate items
      }
    });
  }

  function hiddenItems() {
    //get hidden items
    let items = getCount(item, false);
    let visibleItems = slider.offsetWidth / 210;
    return items - Math.ceil(visibleItems);
  }
}

function translateX(position) {
  //translate items
  slide.style.left = position * -210 + "px";
}

function getCount(parent, getChildrensChildren) {
  //count no of items
  let relevantChildren = 0;
  let children = parent.childNodes.length;
  for (let i = 0; i < children; i++) {
    if (parent.childNodes[i].nodeType != 3) {
      if (getChildrensChildren)
        relevantChildren += getCount(parent.childNodes[i], true);
      relevantChildren++;
    }
  }
  return relevantChildren;
}

